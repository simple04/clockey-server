package com.clockey.server;

import java.util.HashMap;
import java.util.Map;
/**
 * 接口单元测试
 */
import com.clockey.server.util.HttpClientUtil;

public class TestController {
	//private static String baseUrl = "http://localhost:9010";
	private static String baseUrl = "http://app.2ours.cc";
	
	public static void main(String[] args) {
		//testUserLogin();
		testUserGetSecurityCode();
		//testUserRegister();
		
		//testUploadMsg();
		
		//testEventPublish();
	}
	
	//测试用户登录
	public static void testUserLogin(){
		String url = baseUrl+"/clockey/user/login";
		Map<String,Object> requestMap = new HashMap<String,Object>();
		//设置参数
		requestMap.put("phone", "15858233102");
		requestMap.put("password", "3333");
		String responseStr = HttpClientUtil.sendRequest(url, requestMap);
		System.out.println(responseStr);
	}
	
	//测试用户获取验证码
	public static void testUserGetSecurityCode(){
		String url = baseUrl+"/clockey/user/getSecurityCode";
		Map<String,Object> requestMap = new HashMap<String,Object>();
		//设置参数
		requestMap.put("phone", "15858233102");
		String responseStr = HttpClientUtil.sendRequest(url, requestMap);
		System.out.println(responseStr);
	}
	
	//测试用户注册
	public static void testUserRegister(){
		String url = baseUrl+"/clockey/user/register";
		Map<String,Object> requestMap = new HashMap<String,Object>();
		//设置参数
		requestMap.put("phone", "15858233102");
		requestMap.put("securityCode", "3698");//需要上传string类型
		requestMap.put("password", "123456");
		requestMap.put("cid", "dsgsgsgehbtbtebrr");
		String responseStr = HttpClientUtil.sendRequest(url, requestMap);
		System.out.println(responseStr);
	}
	
	//测试回复短信
	public static void testUploadMsg(){
		//String url = "http://localhost:9010/clockey/uploadMsg";//本地测试
		String url = "http://msg.2ours.cc";//线上
		//设置参数
		String uploadMsg = "rrrrrrrrrrrrrrrrrr555555555555555";
		String responseStr = HttpClientUtil.sendRequest(url, uploadMsg,HttpClientUtil.CONTENT_TYPE_XML);
		System.out.println(responseStr);
	}
	
	//测试事件发布
	public static void testEventPublish(){
		String url = baseUrl+"/clockey/event/publish";
		Map<String,Object> requestMap = new HashMap<String,Object>();
		//设置参数
		requestMap.put("phone", "15858233102");
		String responseStr = HttpClientUtil.sendRequest(url, requestMap);
		System.out.println(responseStr);
	}
}
