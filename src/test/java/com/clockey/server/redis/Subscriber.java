package com.clockey.server.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class Subscriber {

    public static void main(String[] args) {
        JedisPool pool = new JedisPool(new JedisPoolConfig(), "23.95.0.152");

        Jedis jedis = pool.getResource();
        //jedis.psubscribe(new KeyExpiredListener(), "__key*__:*");
        jedis.psubscribe(new KeyExpiredListener(), "__keyevent@*__:expired");

    }

}