package com.clockey.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.clockey.server.controller.config.ControllerCacheLoader;
import com.clockey.server.httpserver.Server;
import com.clockey.server.httpserver.util.CustomizedPropertyConfigurer;

public class ServerStart {
	private static final Logger logger = LoggerFactory.getLogger(ServerStart.class);

	public static void main(String[] args) {

		try {
			ApplicationContext context = new ClassPathXmlApplicationContext("main/server.xml");
			ControllerCacheLoader.init(context);//初始化Controller和Method
			Server server = new Server();
			Integer port = Integer.valueOf(CustomizedPropertyConfigurer.getContextProperty("server.port"));
			server.setPort(port);
			server.run();
		} catch (Exception e) {
			logger.error("clockey server start failed: ", e);
			System.exit(-1);
		}
	}

}
