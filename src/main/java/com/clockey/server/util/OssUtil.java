package com.clockey.server.util;

import java.io.InputStream;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectResult;

public class OssUtil {

	private final static String accessKeyId = "";

	private final static String accessKeySecret = "";

	private static OSSClient client = null;

	private static String BUCKET_NAME = "shichen-pic";

	private static String HEAD_PITH = "headPic/";
	
	private static String contentType="image/jpeg";

	static {
		client = new OSSClient(accessKeyId, accessKeySecret);
		if (!client.isBucketExist(BUCKET_NAME)) {
			client.createBucket(BUCKET_NAME);
		}
	}

	public static String uploadImg(InputStream is, String key,
			String contentType, String bucketName) {
		String imgurl = null;
		ObjectMetadata objectMetadata = new ObjectMetadata();
		objectMetadata.setContentType(contentType);
		PutObjectResult result = client.putObject(bucketName, key, is,
				objectMetadata);
		if (result.getETag() != null) {
			imgurl = String.format("http://{0}.oss.aliyuncs.com/{1}",
					bucketName, key);
		}
		return imgurl;
	}

	public static String uploadHeadPic(InputStream is, String key){
		return uploadImg(is,HEAD_PITH+key,contentType,BUCKET_NAME);
	}
	
}
