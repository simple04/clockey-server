package com.clockey.server.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	public final static String YYYY = "yyyy";

	public final static String MM = "MM";

	public final static String DD = "dd";

	public final static String YYYY_MM_DD = "yyyy-MM-dd";

	public final static String YYYY_MM = "yyyy-MM";

	public final static String HH_MM_SS = "HH:mm:ss";

	public final static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

	/**
	 * 获取一个简单的日期格式化对象
	 * 
	 * @return 一个简单的日期格式化对象
	 */
	private static SimpleDateFormat getFormatter(String parttern) {
		return new SimpleDateFormat(parttern);
	}

	/**
	 * 格式化时间
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static String format(Date date, String pattern) {
		if (date == null)
			return "";
		else
			return getFormatter(pattern).format(date);
	}
	
	/**
	 * 计算未来时间跟现在时间的时间差，秒为单位
	 * @param endDate
	 * @return
	 */
	public static long getBetweenTimeFromNow(Date endDate){
		return getBetweenTime(new Date(),endDate);
	}
	/**
	 * 计算两个时间差，秒为单位
	 * @return
	 */
	public static long getBetweenTime(Date startDate,Date endDate){
		long between = (endDate.getTime() - startDate.getTime()) / 1000;// 除以1000是为了转换成秒
		return between;
	}
}
