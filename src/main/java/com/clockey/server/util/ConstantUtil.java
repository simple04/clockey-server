package com.clockey.server.util;

public class ConstantUtil {

	public static final String STATUS_CODE = "statusCode";
	
	public static int SUCCESS = 200;// 成功

	public static int PARAM_INVALID_ERROR = 203;// 参数错误

	/************ 用户 **************/
	public static int USER_ACCOUNT_ERROR = 1001;// 帐号或密码错误
	public static int USER_SECURITY_CODE_ERROR = 1002;// 验证码错误
	public static int USER_ACCOUNT_NOT_FOUND = 1003;// 帐号不存在
	/************ 用户 **************/

	/***** redis ******/
	public static final String KEY_PREFIX_SCHEDULER = "scheduler";
	public static final String KEY_PREFIX_SENDED_USER = "sendedMsgUser-";
	public static final String KEY_PREFIX_SECURITY_CODE = "scode-";
}
