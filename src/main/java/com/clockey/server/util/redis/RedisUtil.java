package com.clockey.server.util.redis;

import com.clockey.server.httpserver.util.CustomizedPropertyConfigurer;
import com.clockey.server.schedule.KeyExpiredListener;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public final class RedisUtil {

	// Redis服务器IP
	private static String ADDR = CustomizedPropertyConfigurer.getContextProperty("redis.address");

	// Redis的端口号
	private static int PORT = Integer.valueOf(CustomizedPropertyConfigurer.getContextProperty("redis.port"));

	// 访问密码
	// private static String AUTH = "admin";

	// 可用连接实例的最大数目，默认值为8；
	// 如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。
	private static int MAX_TOTAL = Integer.valueOf(CustomizedPropertyConfigurer.getContextProperty("redis.maxTotal"));

	// 控制一个pool最多有多少个状态为idle(空闲的)的jedis实例，默认值也是8。
	private static int MAX_IDLE = Integer.valueOf(CustomizedPropertyConfigurer.getContextProperty("redis.maxIdle"));

	// 等待可用连接的最大时间，单位毫秒，默认值为-1，表示永不超时。如果超过等待时间，则直接抛出JedisConnectionException；
	private static int MAX_WAIT_MILLIS = Integer.valueOf(CustomizedPropertyConfigurer.getContextProperty("redis.maxWaitMillis"));

	private static int TIMEOUT = Integer.valueOf(CustomizedPropertyConfigurer.getContextProperty("redis.timeout"));

	// 在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
	private static boolean TEST_ON_BORROW = Boolean.valueOf(CustomizedPropertyConfigurer.getContextProperty("redis.testOnBorrow"));

	private static JedisPool jedisPool = null;

	/**
	 * 初始化Redis连接池
	 */
	static {
		try {
			JedisPoolConfig config = new JedisPoolConfig();
			config.setMaxTotal(MAX_TOTAL);
			config.setMaxIdle(MAX_IDLE);
			config.setMaxWaitMillis(MAX_WAIT_MILLIS);
			config.setTestOnBorrow(TEST_ON_BORROW);
			jedisPool = new JedisPool(config, ADDR, PORT, TIMEOUT);// , AUTH);
			//为了计划任务
			new Thread(new Runnable() {
				@Override
				public void run() {
					Jedis jedis = jedisPool.getResource();
					jedis.psubscribe(new KeyExpiredListener(), "__keyevent@*__:expired");
				}

			}).start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获取Jedis实例
	 * 
	 * @return
	 */
	public synchronized static Jedis getJedis() {
		try {
			if (jedisPool != null) {
				Jedis resource = jedisPool.getResource();
				return resource;
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 释放jedis资源
	 * 
	 * @param jedis
	 */
	public static void close(final Jedis jedis) {
		if (jedis != null) {
			jedis.close();
		}
	}

	/**
	 * 释放jedis资源
	 * 
	 * @param jedis
	 */
	// public static void returnResource(final Jedis jedis) {
	// if (jedis != null) {
	// jedisPool.returnResource(jedis);
	// }
	// }
}