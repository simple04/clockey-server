package com.clockey.server.util;

import java.util.Random;

public class SecurityCodeUtil {

	public static String genSecurityCode(){
		StringBuffer randomCode=new StringBuffer();
		Random random=new Random(); 
		for (int i=0;i<4;i++)
        {
            //得到随机产生的验证码数字。
            String strRand=String.valueOf(random.nextInt(10));
            //将产生的四个随机数组合在一起。
            randomCode.append(strRand);
        }
		return randomCode.toString();
	}
}
