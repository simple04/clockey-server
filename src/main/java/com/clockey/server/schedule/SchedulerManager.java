package com.clockey.server.schedule;

import redis.clients.jedis.Jedis;

import com.clockey.server.util.ConstantUtil;
import com.clockey.server.util.redis.RedisUtil;

public class SchedulerManager {

//	private static JedisPool pool = null;
//	private static String redisHost = "23.95.0.152";
//	
//	static {
//		pool = new JedisPool(new JedisPoolConfig(), redisHost);
//		new Thread(new Runnable(){
//			@Override
//			public void run() {
//				Jedis jedis = pool.getResource();
//				jedis.psubscribe(new KeyExpiredListener(), "__keyevent@*__:expired");
//			}
//			
//		}).start();
//	}

	/**
	 * 
	 * @param eventId 发布的事件ID
	 * @param delaySeconds 多少秒后发生
	 */
	public static void addEventNotice(Long eventId, int delaySeconds,int advanceTime) {
		Jedis jedis = RedisUtil.getJedis();
		String id = ConstantUtil.KEY_PREFIX_SCHEDULER+"+"+eventId+"+"+advanceTime;//scheduler+112+30
		jedis.set(id, "");
		jedis.expire(id, delaySeconds);
		jedis.close();
	}
}
