package com.clockey.server.schedule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import redis.clients.jedis.Jedis;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.clockey.server.service.EventService;
import com.clockey.server.util.ConstantUtil;
import com.clockey.server.util.message.MessageUtil;
import com.clockey.server.util.redis.RedisUtil;
import com.cloopen.rest.sdk.CCPRestSmsSDK;
import com.gexin.rp.sdk.base.IBatch;
import com.gexin.rp.sdk.base.IPushResult;
import com.gexin.rp.sdk.base.impl.SingleMessage;
import com.gexin.rp.sdk.base.impl.Target;
import com.gexin.rp.sdk.http.IGtPush;
import com.gexin.rp.sdk.template.LinkTemplate;

public class EventNoticer {

	private String appId = "b03c5cfef65ed30108f0a3fd82c3f6b4";
	private String appkey = "110000";
	private String master = "a02a76119b20d4e31620d7597a3b4f35";
	private String host = "http://sdk.open.api.igexin.com/apiex.htm";

	@Autowired
	private EventService eventService;

	public void noticeEventPaticipantsByEventId(Long eventId,String advanceTimeStr) {
		List<String> cids = new ArrayList<String>();
		// List<String> phones = new ArrayList<String>();
		// 取出所有参与者
		JSONArray eventParticipants = eventService.getEventParticipantsByEventId(eventId);
		Jedis jedis = RedisUtil.getJedis();

		for (int i = 0; i < eventParticipants.size(); i++) {
			JSONObject userObject = eventParticipants.getJSONObject(i);
			String cid = (String) userObject.get("cid");
			cids.add(cid);
			String phone = (String) userObject.get("phone");
			// phones.add(phone);
			// 发送短信
			CCPRestSmsSDK restAPI = MessageUtil.getSender();
			HashMap<String, Object> result = restAPI.sendTemplateSMS(phone, "59682", new String[] { "test", advanceTimeStr });

			if ("000000".equals(result.get("statusCode"))) {
				//发生成功就将该用户加入缓存
				String redisKey = ConstantUtil.KEY_PREFIX_SENDED_USER + phone;
				jedis.set(redisKey, userObject.toJSONString());
				jedis.expire(redisKey, 7*24*60*60);//缓存有效期7天
			} else {
				System.out.println("错误码=" + result.get("statusCode") + " 错误信息= " + result.get("statusMsg"));
			}
		}
		jedis.close();//不要忘了返回池子
		// 发送推送
		push2Clients(cids);
	}

	/**
	 * 推送给客户数
	 * 
	 * @param cids
	 * @throws Exception
	 */
	private void push2Clients(List<String> cids) {
		System.setProperty("gexin_pushSingleBatch_needAsync", "false");
		IGtPush push = new IGtPush(host, appkey, master);
		IBatch batch = push.getBatch();

		LinkTemplate template = new LinkTemplate();
		template.setAppId(appId);
		template.setAppkey(appkey);
		template.setTitle("标题");
		template.setText("文本");
		template.setLogo("text.png");
		template.setUrl("http://www.baidu.com");

		SingleMessage message = new SingleMessage();
		message.setOffline(true);
		message.setOfflineExpireTime(2 * 1000 * 3600);
		message.setData(template);

		for (String cid : cids) {
			Target target = new Target();
			target.setAppId(appId);
			target.setClientId(cid);
			try {
				batch.add(message, target);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		try {
			String result = batch.submit().getResponse().toString();
			System.out.println(result);
		} catch (Exception e) {
			IPushResult ret = null;
			try {
				ret = batch.retry();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out.println("异常：" + ret.getResponse().toString());
		}

	}

}
