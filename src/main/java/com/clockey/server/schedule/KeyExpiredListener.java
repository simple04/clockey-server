package com.clockey.server.schedule;

import org.apache.commons.lang.StringUtils;

import redis.clients.jedis.JedisPubSub;

import com.clockey.server.util.ConstantUtil;

public class KeyExpiredListener extends JedisPubSub {

	@Override
	public void onPSubscribe(String pattern, int subscribedChannels) {
		// System.out.println("onPSubscribe " + pattern + " " +
		// subscribedChannels);
	}

	@Override
	public void onPMessage(String pattern, String channel, String message) {
		// System.out.println("onPMessage pattern " + pattern + " " + channel +
		// " " + message);
		if (StringUtils.startsWith(message, ConstantUtil.KEY_PREFIX_SCHEDULER)) {//scheduler+112+30
			String[] array = StringUtils.split(message,"+");
			Long eventId = Long.valueOf(array[1]);
			String advanceTimeStr = array[2];
			//String eventIdStr = StringUtils.substring(message,StringUtils.indexOf(message, ConstantUtil.SCHEDULER_PREFIX) + StringUtils.length(ConstantUtil.SCHEDULER_PREFIX));
			new EventNoticer().noticeEventPaticipantsByEventId(eventId,advanceTimeStr);
		}
	}

}