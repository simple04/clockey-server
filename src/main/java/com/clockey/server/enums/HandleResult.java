package com.clockey.server.enums;

public enum HandleResult {
	REFUSE(2), UNHAND(0), APPROVE(1);

	private int result;

	HandleResult(int result) {
		this.result = result;
	}

	public int getResult() {
		return result;
	}

}
