package com.clockey.server.httpserver.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.clockey.server.httpserver.netty.mvc.method.ControllerMethod;

public class ControllerMethodCache {
	private static Map<String, ControllerMethod> controllerMethodCache = new ConcurrentHashMap<String, ControllerMethod>();

	public static Map<String, ControllerMethod> getControllerMethodCache() {
		return controllerMethodCache;
	}

	public static void setControllerMethodCache(Map<String, ControllerMethod> controllerMethodCache) {
		ControllerMethodCache.controllerMethodCache = controllerMethodCache;
	}

}
