package com.clockey.server.httpserver.netty.mvc.method;

import com.clockey.server.httpserver.netty.Request;
import com.clockey.server.httpserver.netty.bean.Modle;
import com.clockey.server.httpserver.netty.mvc.Controller;
/**
 * 只用 public void execute(Request request,Map<String, Object> responseMap) throws Exception
 * @author littleBirdTao
 *
 */
public abstract class ControllerMethodAbs implements ControllerMethod {

	private Controller controller;

	/**
	 * 重写下,为了使继承他的不再重写
	 */
	public void execute(Request request,Modle modle){
		return;
	}
	
	public ControllerMethodAbs(Controller controller) {
		this.controller = controller;
	}

	public Controller getController() {
		return controller;
	}

	public void setController(Controller controller) {
		this.controller = controller;
	}

}
