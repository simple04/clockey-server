package com.clockey.server.httpserver.netty.bean;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.HttpResponseStatus;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;

public class Modle {
	private static final Logger logger = LoggerFactory.getLogger(Modle.class);

	public static final String CONTENT_TYPE_HTML = "text/html";
	public static final String CONTENT_TYPE_JSON = "application/json";
	public static final String CONTENT_TYPE_XML = "text/xml";

	private String charset = "utf-8";
	private Map<String, Object> contentMap = new HashMap<String, Object>();// 最终转换为contentByteBuf
	private ByteBuf contentByteBuf; //= Unpooled.EMPTY_BUFFER;下面要判断是否null,所以注释
	private String contentType = CONTENT_TYPE_JSON;
	private HttpResponseStatus httpResponseStatus = HttpResponseStatus.OK;

	public String getCharset() {
		return charset;
	}

	public Modle setCharset(String charset) {
		this.charset = charset;
		return this;
	}

	public ByteBuf getContent() {
		if (contentByteBuf == null) {
			setContent(JSON.toJSONString(contentMap));
		}
		return contentByteBuf;
	}

	public Modle setContent(String content) {
		if (content == null) {
			logger.info("returned modle's content is null");
			return this;
		}
		this.contentByteBuf = Unpooled.wrappedBuffer(content.getBytes(Charset.forName(charset)));
		return this;
	}

	public Modle setContent(byte[] content) {
		if (content == null) {
			logger.info("returned modle's content is null");
			return this;
		}
		this.contentByteBuf = Unpooled.wrappedBuffer(content);
		return this;
	}

	public String getContentType() {
		return contentType;
	}

	public Modle setContentType(String contentType) {
		this.contentType = contentType;
		return this;
	}

	public HttpResponseStatus getHttpResponseStatus() {
		return httpResponseStatus;
	}

	public Modle setHttpResponseStatus(HttpResponseStatus httpResponseStatus) {
		this.httpResponseStatus = httpResponseStatus;
		return this;
	}

	public Map<String, Object> getContentMap() {
		return contentMap;
	}

	public void setContentMap(Map<String, Object> contentMap) {
		this.contentMap = contentMap;
	}

}
