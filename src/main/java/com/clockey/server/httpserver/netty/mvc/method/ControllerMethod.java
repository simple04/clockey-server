package com.clockey.server.httpserver.netty.mvc.method;

import java.util.Map;

import com.clockey.server.httpserver.netty.Request;
import com.clockey.server.httpserver.netty.bean.Modle;

public interface ControllerMethod {
	public void execute(Request request,Map<String, Object> responseMap) throws Exception;
	public void execute(Request request,Modle modle) throws Exception;
}
