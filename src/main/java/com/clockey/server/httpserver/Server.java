package com.clockey.server.httpserver;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.codec.http.HttpServerCodec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.clockey.server.httpserver.netty.HttpServerHandler;

/**
 * start http server
 * 
 * @author littleBirdTao
 *
 */
public class Server {
	private static final Logger logger = LoggerFactory.getLogger(Server.class);
	/** listening port **/
	private int port;

	public void run() throws Exception {
		EventLoopGroup bossGroup = new NioEventLoopGroup();
		EventLoopGroup workerGroup = new NioEventLoopGroup();

		try {
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class).childHandler(new ChannelInitializer<SocketChannel>() {
				@Override
				protected void initChannel(SocketChannel ch) throws Exception {
					ChannelPipeline pipeline = ch.pipeline();
					pipeline.addLast(new HttpRequestDecoder());
					// pipeline.addLast(new HttpObjectAggregator(1024 * 1024 *
					// 64));//for FullHttpRequest
					pipeline.addLast(new HttpServerCodec());
					pipeline.addLast(new HttpServerHandler());
					pipeline.addLast(new HttpResponseEncoder());
				}
			}).option(ChannelOption.SO_BACKLOG, 1024);

			ChannelFuture f = b.bind(port).sync();
			logger.info("clockey server is running on port:{}", port);
			f.channel().closeFuture().sync();
		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

}
