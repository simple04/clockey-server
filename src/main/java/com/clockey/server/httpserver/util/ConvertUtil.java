package com.clockey.server.httpserver.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSONObject;

public class ConvertUtil {

	/**
	 * json to Map
	 * @param json
	 * @return
	 */
	public static Map<String, Object> json2Map(String json) {
		Map<String, Object> result = null;
		if (StringUtils.isNotBlank(json)) {
			JSONObject jsonObject = null;
			try{
				jsonObject = JSONObject.parseObject(json);
			}catch(Exception e){
				e.printStackTrace();
			}
			if(jsonObject != null){
				result = new HashMap<String, Object>();
				Set<String> keys = jsonObject.keySet();
				for (String key : keys) {
					result.put(key, jsonObject.get(key));
				}
			}
		}
		return result;
	}
	
}
