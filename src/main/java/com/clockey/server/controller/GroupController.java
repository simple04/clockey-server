package com.clockey.server.controller;

import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.clockey.server.httpserver.netty.Request;
import com.clockey.server.httpserver.netty.mvc.Controller;
import com.clockey.server.pojo.Group;
import com.clockey.server.service.GroupService;
import com.clockey.server.util.ConstantUtil;

public class GroupController implements Controller {

	private GroupService groupService;


	public void save(Request request, Map<String, Object> responseMap) {
		Map<String, Object> map = request.getParameterMap();
		JSONObject jsonObject = new JSONObject(map);
		Group group = JSON.parseObject(jsonObject.toString(), Group.class);
		groupService.save(group);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
	}

	public void list(Request request, Map<String, Object> responseMap) {
		Map<String, Object> map = request.getParameterMap();
		JSONArray result = groupService.list(map);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
		responseMap.put("groups", result);
	}

	public void delete(Request request, Map<String, Object> responseMap) {
		Map<String, Object> map = request.getParameterMap();
		groupService.delete(map);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
	}

	public void update(Request request, Map<String, Object> responseMap) {
		Map<String, Object> map = request.getParameterMap();
		JSONObject jsonObject = new JSONObject(map);
		Group group = JSON.parseObject(jsonObject.toString(), Group.class);
		groupService.update(group);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
	}

	public GroupService getGroupService() {
		return groupService;
	}

	public void setGroupService(GroupService groupService) {
		this.groupService = groupService;
	}
	
}
