package com.clockey.server.controller;

import java.util.Date;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.clockey.server.httpserver.netty.Request;
import com.clockey.server.httpserver.netty.mvc.Controller;
import com.clockey.server.pojo.Event;
import com.clockey.server.schedule.SchedulerManager;
import com.clockey.server.service.EventService;
import com.clockey.server.util.ConstantUtil;

/**
 * 不受Spring托管
 * 
 * @author littleBirdTao
 *
 */
public class EventController implements Controller {

	private EventService eventService;

	/**
	 * 发布活动
	 * 
	 * @param request
	 * @return
	 */
	public void publishEvent(Request request, Map<String, Object> responseMap) {
		Map<String, Object> paramMap = request.getParameterMap();
		eventService.publishEvent(paramMap);
		Event event = (Event) paramMap.get("event");
		Date beginTime = event.getBeginTime();//活动开始时间
		int advanceTime = event.getAdvanceTime();//提前确认时间,单位分钟
		Long delaySeconds = (beginTime.getTime() - advanceTime*60*1000 - (new Date()).getTime()) / 1000;
		SchedulerManager.addEventNotice(event.getId(), delaySeconds.intValue(),advanceTime);// 注意long int
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
	}

	public void listEvents(Request request, Map<String, Object> responseMap) {
		Map<String, Object> map = request.getParameterMap();
		JSONArray jsonArray = eventService.getEventsByCondition(map);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
		responseMap.put("events", jsonArray.toString());
	}

	public void listMyPublishEvents(Request request,
			Map<String, Object> responseMap) {
		Map<String, Object> map = request.getParameterMap();
		JSONArray jsonArray = eventService.getPublisherEventsByUserId(map);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
		responseMap.put("events", jsonArray.toString());
	}

	public void listMyParticipantEvents(Request request,
			Map<String, Object> responseMap) {
		Map<String, Object> map = request.getParameterMap();
		JSONArray jsonArray = eventService.getParticipantEventsByUserId(map);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
		responseMap.put("events", jsonArray.toString());
	}

	public void listParticipants(Request request,
			Map<String, Object> responseMap) {
		Long eventId = Long.parseLong(request.getParameter("eventId"));
		JSONArray jsonArray = eventService
				.getEventParticipantsByEventId(eventId);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
		responseMap.put("participants", jsonArray.toString());
	}

	public void getEvent(Request request, Map<String, Object> responseMap) {
		Map<String, Object> map = request.getParameterMap();
		JSONObject jsonObject = eventService.getEventById(map);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
		responseMap.put("event", jsonObject.toString());
	}

	public void handEventByInviter(Request request,
			Map<String, Object> responseMap) {
		Map<String, Object> map = request.getParameterMap();
		eventService.handEventByInviter(map);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
	}

	public void participantEvent(Request request,
			Map<String, Object> responseMap) {
		Map<String, Object> map = request.getParameterMap();
		eventService.participantEvent(map);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
	}

	public EventService getEventService() {
		return eventService;
	}

	public void setEventService(EventService eventService) {
		this.eventService = eventService;
	}

}
