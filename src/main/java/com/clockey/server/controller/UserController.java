package com.clockey.server.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import redis.clients.jedis.Jedis;

import com.clockey.server.httpserver.netty.Request;
import com.clockey.server.httpserver.netty.mvc.Controller;
import com.clockey.server.pojo.User;
import com.clockey.server.service.UserService;
import com.clockey.server.util.ConstantUtil;
import com.clockey.server.util.SecurityCodeUtil;
import com.clockey.server.util.message.MessageUtil;
import com.clockey.server.util.redis.RedisUtil;
import com.cloopen.rest.sdk.CCPRestSmsSDK;

/**
 * 不受Spring托管
 * 
 * @author littleBirdTao
 *
 */
public class UserController implements Controller {

	private UserService userService;

	/**
	 * 用户登录
	 * 
	 * @param request
	 * @param modle
	 */
	public void login(Request request, Map<String, Object> responseMap) {
		Map<String, Object> paramMap = request.getParameterMap();
		String uploadPhone = (String) paramMap.get("phone");// 上传的手机号码
		User user = userService.getUserByPhone(uploadPhone);
		if(user == null){
			responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.USER_ACCOUNT_NOT_FOUND);// 帐号不存在
			return;
		}
		String uploadPassword = (String) paramMap.get("password");
		if (StringUtils.equals(user.getPassword(), uploadPassword)) {
			responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);// 帐号密码验证成功
		} else {
			responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.USER_ACCOUNT_ERROR);// 帐号或密码错误
		}
	}

	public void getSecurityCode(Request request, Map<String, Object> responseMap) {
		Map<String, Object> paramMap = request.getParameterMap();
		String uploadPhone = StringUtils.trim((String) paramMap.get("phone"));// 上传的手机号码
		// 获取验证码测试
		CCPRestSmsSDK restAPI = MessageUtil.getSender();
		String securityCode = SecurityCodeUtil.genSecurityCode();// 取得四位数字随机数securityCode
		HashMap<String, Object> result = restAPI.sendTemplateSMS(uploadPhone, "59680", new String[] { securityCode, "5" });

		if ("000000".equals(result.get("statusCode"))) {
			// 将生成的securityCode放到缓存中
			Jedis jedis = RedisUtil.getJedis();
			String key = ConstantUtil.KEY_PREFIX_SECURITY_CODE+uploadPhone;
			jedis.set(key, securityCode);
			jedis.expire(key, 300);//5分后失效
			jedis.close();
			//UserSecurityCodeMap.getUserSecurityCodeMap().put(uploadPhone, securityCode);
			responseMap.put("statusCode", ConstantUtil.SUCCESS);
		} else {
			System.out.println("错误码=" + result.get("statusCode") + " 错误信息= " + result.get("statusMsg"));
			responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.PARAM_INVALID_ERROR);
		}
	}

	/**
	 * 新用户注册
	 * 
	 * @param request
	 * @param modle
	 */
	public void register(Request request, Map<String, Object> responseMap) {
		Map<String, Object> paramMap = request.getParameterMap();
		String securityCode = (String) paramMap.get("securityCode");
		String phone = (String) paramMap.get("phone");
		String password = (String) paramMap.get("password");
		String cid = (String) paramMap.get("cid");
		
		String key = ConstantUtil.KEY_PREFIX_SECURITY_CODE+phone;
		Jedis jedis = RedisUtil.getJedis();
		String cacheSecurityCode = jedis.get(key);
		if (StringUtils.equals(securityCode, cacheSecurityCode)) {
			User user = new User();
			user.setPhone(phone);
			user.setPassword(password);
			user.setCid(cid);
			userService.save(user);
			jedis.del(key);//删除缓存
			responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
		} else {
			responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.USER_SECURITY_CODE_ERROR);
		}
		jedis.close();
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
