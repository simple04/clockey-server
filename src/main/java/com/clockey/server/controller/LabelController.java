package com.clockey.server.controller;

import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.clockey.server.httpserver.netty.Request;
import com.clockey.server.httpserver.netty.mvc.Controller;
import com.clockey.server.pojo.Label;
import com.clockey.server.service.LabelService;
import com.clockey.server.util.ConstantUtil;

public class LabelController implements Controller{

	private LabelService labelService;

	public LabelService getLabelService() {
		return labelService;
	}

	public void setLabelService(LabelService labelService) {
		this.labelService = labelService;
	}
	
	
	public void save(Request request, Map<String, Object> responseMap){
		Map<String, Object> map = request.getParameterMap();
		JSONObject jsonObject = new JSONObject(map);
		Label label = JSON.parseObject(jsonObject.toString(), Label.class);
		labelService.save(label);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
	}
	
	public void getLabelByKey(Request request, Map<String, Object> responseMap){
		Map<String, Object> map = request.getParameterMap();
		JSONArray jsonArray = labelService.findLabel(map);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
		responseMap.put("labels", jsonArray.toString());
	}
	
	
	
	
	
	
	
}
