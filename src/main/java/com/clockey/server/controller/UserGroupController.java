package com.clockey.server.controller;

import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.clockey.server.httpserver.netty.Request;
import com.clockey.server.httpserver.netty.mvc.Controller;
import com.clockey.server.service.UserGroupService;
import com.clockey.server.util.ConstantUtil;

public class UserGroupController implements Controller{

	
	private UserGroupService userGroupService;

	public UserGroupService getUserGroupService() {
		return userGroupService;
	}

	public void setUserGroupService(UserGroupService userGroupService) {
		this.userGroupService = userGroupService;
	}
	
	public void saveAll(Request request,
			Map<String, Object> responseMap){
		Map<String, Object> map = request.getParameterMap();
		userGroupService.saveAll(map);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
	}
	
	public void deleteUserIds(Request request,
			Map<String, Object> responseMap){
		Map<String, Object> map = request.getParameterMap();
		userGroupService.deleteUserIds(map);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
	}
	
	public void getUsersByGroup(Request request,
			Map<String, Object> responseMap){
		Map<String, Object> map = request.getParameterMap();
		JSONArray result = userGroupService.getUsersByGroup(map);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
		responseMap.put("users", result);
	}
}
