package com.clockey.server.controller.config;

import java.util.Map;

import org.springframework.context.ApplicationContext;

import com.clockey.server.controller.EventController;
import com.clockey.server.controller.FriendController;
import com.clockey.server.controller.GroupController;
import com.clockey.server.controller.LabelController;
import com.clockey.server.controller.UploadMsgController;
import com.clockey.server.controller.UserController;
import com.clockey.server.controller.UserGroupController;
import com.clockey.server.httpserver.cache.ControllerMethodCache;
import com.clockey.server.httpserver.netty.Request;
import com.clockey.server.httpserver.netty.bean.Modle;
import com.clockey.server.httpserver.netty.mvc.method.ControllerMethod;
import com.clockey.server.httpserver.netty.mvc.method.ControllerMethodAbs;
import com.clockey.server.httpserver.netty.mvc.method.ControllerMethodOrigiAbs;
import com.clockey.server.service.EventService;
import com.clockey.server.service.FriendService;
import com.clockey.server.service.GroupService;
import com.clockey.server.service.LabelService;
import com.clockey.server.service.UserGroupService;
import com.clockey.server.service.UserService;

public class ControllerCacheLoader {
	public static Map<String, ControllerMethod> controllerMethodCache = ControllerMethodCache.getControllerMethodCache();

	public static void init(ApplicationContext context) {

		// UserController开始*******************
		final UserController userController = new UserController();
		userController.setUserService((UserService) context.getBean("userServiceImpl"));

		controllerMethodCache.put("/clockey/user/login", new ControllerMethodAbs(userController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				userController.login(request, responseMap);
			}
		});
		controllerMethodCache.put("/clockey/user/getSecurityCode", new ControllerMethodAbs(userController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				userController.getSecurityCode(request, responseMap);
			}
		});
		controllerMethodCache.put("/clockey/user/register", new ControllerMethodAbs(userController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				userController.register(request, responseMap);
			}
		});

		// UploadMsgController开始
		final UploadMsgController uploadMsgController = new UploadMsgController();
		uploadMsgController.setEventService((EventService) context.getBean("eventServiceImpl"));
		controllerMethodCache.put("/clockey/uploadMsg", new ControllerMethodOrigiAbs(uploadMsgController) {
			@Override
			public void execute(Request request, Modle modle) throws Exception {
				uploadMsgController.handleMsg(request, modle);
			}
		});

		// EventController开始*******************
		final EventController eventController = new EventController();
		eventController.setEventService((EventService) context.getBean("eventServiceImpl"));

		controllerMethodCache.put("/clockey/event/publishEvent", new ControllerMethodAbs(eventController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				eventController.publishEvent(request, responseMap);
			}
		});
		controllerMethodCache.put("/clockey/event/listEvents", new ControllerMethodAbs(eventController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				eventController.listEvents(request, responseMap);
			}
		});
		controllerMethodCache.put("/clockey/event/listMyPublishEvents", new ControllerMethodAbs(eventController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				eventController.listMyPublishEvents(request, responseMap);
			}
		});
		controllerMethodCache.put("/clockey/event/listMyParticipantEvents", new ControllerMethodAbs(eventController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				eventController.listMyParticipantEvents(request, responseMap);
			}
		});
		controllerMethodCache.put("/clockey/event/listParticipants", new ControllerMethodAbs(eventController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				eventController.listParticipants(request, responseMap);
			}
		});
		controllerMethodCache.put("/clockey/event/getEvent", new ControllerMethodAbs(eventController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				eventController.getEvent(request, responseMap);
			}
		});
		controllerMethodCache.put("/clockey/event/handEventByInviter", new ControllerMethodAbs(eventController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				eventController.handEventByInviter(request, responseMap);
			}
		});
		controllerMethodCache.put("/clockey/event/participantEvent", new ControllerMethodAbs(eventController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				eventController.participantEvent(request, responseMap);
			}
		});

		// GroupController开始*******************
		final GroupController groupController = new GroupController();
		groupController.setGroupService((GroupService) context.getBean("groupServiceImpl"));
		controllerMethodCache.put("/clockey/group/save", new ControllerMethodAbs(groupController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				groupController.save(request, responseMap);
			}
		});
		controllerMethodCache.put("/clockey/group/update", new ControllerMethodAbs(groupController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				groupController.update(request, responseMap);
			}
		});
		controllerMethodCache.put("/clockey/group/delete", new ControllerMethodAbs(groupController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				groupController.delete(request, responseMap);
			}
		});
		controllerMethodCache.put("/clockey/group/list", new ControllerMethodAbs(groupController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				groupController.list(request, responseMap);
			}
		});

		// FriendController开始*******************
		final FriendController friendController = new FriendController();
		friendController.setFriendService((FriendService) context.getBean("friendServiceImpl"));

		controllerMethodCache.put("/clockey/friend/request", new ControllerMethodAbs(friendController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				friendController.sendAddFriendRequest(request, responseMap);
			}
		});
		controllerMethodCache.put("/clockey/friend/delete", new ControllerMethodAbs(friendController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				friendController.deleteFriends(request, responseMap);
			}
		});
		controllerMethodCache.put("/clockey/friend/handle", new ControllerMethodAbs(friendController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				friendController.handAddFriendRequest(request, responseMap);
			}
		});
		controllerMethodCache.put("/clockey/friend/list", new ControllerMethodAbs(friendController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				friendController.list(request, responseMap);
			}
		});

		// LabelController开始*******************
		final LabelController labelController = new LabelController();
		labelController.setLabelService((LabelService) context.getBean("labelServiceImpl"));

		controllerMethodCache.put("/clockey/label/save", new ControllerMethodAbs(labelController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				labelController.save(request, responseMap);
			}
		});
		controllerMethodCache.put("/clockey/label/search", new ControllerMethodAbs(labelController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				labelController.getLabelByKey(request, responseMap);
			}
		});

		// UserGroupController开始*******************
		final UserGroupController userGroupController = new UserGroupController();
		userGroupController.setUserGroupService((UserGroupService) context.getBean("userGroupServiceImpl"));

		controllerMethodCache.put("/clockey/userGroup/saveall", new ControllerMethodAbs(userGroupController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				userGroupController.saveAll(request, responseMap);
			}
		});
		controllerMethodCache.put("/clockey/userGroup/deleteuser", new ControllerMethodAbs(userGroupController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				userGroupController.deleteUserIds(request, responseMap);
			}
		});
		controllerMethodCache.put("/clockey/userGroup/getusers", new ControllerMethodAbs(userGroupController) {
			@Override
			public void execute(Request request, Map<String, Object> responseMap) {
				userGroupController.getUsersByGroup(request, responseMap);
			}
		});
	}
}
