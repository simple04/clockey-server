package com.clockey.server.controller;

import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.clockey.server.httpserver.netty.Request;
import com.clockey.server.httpserver.netty.mvc.Controller;
import com.clockey.server.service.FriendService;
import com.clockey.server.util.ConstantUtil;

public class FriendController implements Controller {

	private FriendService friendService;

	public FriendService getFriendService() {
		return friendService;
	}

	public void setFriendService(FriendService friendService) {
		this.friendService = friendService;
	}

	public void sendAddFriendRequest(Request request,
			Map<String, Object> responseMap) {
		Map<String, Object> map = request.getParameterMap();
		friendService.sendAddFriendRequest(map);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
	}

	public void handAddFriendRequest(Request request,
			Map<String, Object> responseMap) {
		Map<String, Object> map = request.getParameterMap();
		friendService.handAddFriendRequest(map);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
	}

	public void deleteFriends(Request request, Map<String, Object> responseMap) {
		Map<String, Object> map = request.getParameterMap();
		friendService.deleteFriends(map);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
	}

	public void list(Request request, Map<String, Object> responseMap) {
		Map<String, Object> map = request.getParameterMap();
		JSONArray result = friendService.list(map);
		responseMap.put(ConstantUtil.STATUS_CODE, ConstantUtil.SUCCESS);
		responseMap.put("friends", result);
	}
}
