package com.clockey.server.dao;

import com.clockey.server.base.BaseMapper;
import com.clockey.server.pojo.User;

public interface UserMapper extends BaseMapper<User>{

}
