package com.clockey.server.dao;

import com.clockey.server.base.BaseMapper;
import com.clockey.server.pojo.UserGroupRel;

public interface UserGroupMapper extends BaseMapper<UserGroupRel>{

}
