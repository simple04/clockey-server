package com.clockey.server.dao;

import com.clockey.server.base.BaseMapper;
import com.clockey.server.pojo.Group;

public interface GroupMapper extends BaseMapper<Group>{

}
