package com.clockey.server.dao;

import java.util.List;

import com.clockey.server.base.BaseMapper;
import com.clockey.server.pojo.Event;

public interface EventMapper extends BaseMapper<Event> {

	/**
	 * 根据id查找事件
	 * @param ids
	 * @return
	 */
	public List<Event> listByids(List<Long>ids);
	
}
