package com.clockey.server.dao;

import com.clockey.server.base.BaseMapper;
import com.clockey.server.pojo.FriendRequest;

public interface FriendRequestMapper extends BaseMapper<FriendRequest>{

}
