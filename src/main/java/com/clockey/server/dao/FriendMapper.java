package com.clockey.server.dao;

import com.clockey.server.base.BaseMapper;
import com.clockey.server.pojo.Friend;

public interface FriendMapper extends BaseMapper<Friend>{

}
