package com.clockey.server.dao;

import java.util.List;
import java.util.Map;

import com.clockey.server.base.BaseMapper;
import com.clockey.server.pojo.EventPublisher;

public interface EventPublisherMapper extends BaseMapper<EventPublisher> {

	/**
	 * 根据用户id查询查询事件
	 * @param userId
	 * @return
	 */
	public List<Long> selectListByUserId(Map<String,Object>map);
	
}
