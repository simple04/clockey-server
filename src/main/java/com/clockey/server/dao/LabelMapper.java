package com.clockey.server.dao;

import java.util.List;

import com.clockey.server.base.BaseMapper;
import com.clockey.server.pojo.Label;

public interface LabelMapper extends BaseMapper<Label> {

	public List<String> selectLabelNameByEventId(Long eventId);
}
