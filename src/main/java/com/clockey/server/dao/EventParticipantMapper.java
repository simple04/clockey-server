package com.clockey.server.dao;

import com.clockey.server.base.BaseMapper;
import com.clockey.server.pojo.EventParticipant;

public interface EventParticipantMapper extends BaseMapper<EventParticipant> {
	
}
