package com.clockey.server.service;

import java.util.Map;

import com.alibaba.fastjson.JSONArray;


public interface UserGroupService {
	/**
	 * 保存用户和组的关系
	 * @param map
	 */
	public void saveAll(Map<String,Object>map);
	
	/**
	 * 把用户移除出组
	 * @param map
	 */
	public void deleteUserIds(Map<String,Object>map);
	
	/**
	 * 
	 * @param map
	 */
	public JSONArray getUsersByGroup(Map<String,Object>map);
	
}
