package com.clockey.server.service;

import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.clockey.server.pojo.Label;

public interface LabelService {

	/**
	 * 保存标签
	 * @param label
	 * @return
	 */
	public long save(Label label);
	
	/**
	 * 模糊查找标签
	 * @param map
	 * @return
	 */
	public JSONArray findLabel(Map<String,Object> map);
}
