package com.clockey.server.service;

import java.util.Map;

import com.alibaba.fastjson.JSONArray;

public interface FriendService {
	
	/**
	 * 添加好友
	 * @param map
	 */
	public void sendAddFriendRequest(Map<String,Object>map);
	
	/**
	 * 处理添加好友请求
	 * @param map
	 */
	public void handAddFriendRequest(Map<String,Object>map);
	
	/**
	 * 删除好友
	 * @param map
	 */
	public void deleteFriends(Map<String,Object>map);
	
	/**
	 * 获取好友列表
	 * @param map
	 * @return
	 */
	public JSONArray list(Map<String,Object>map);

}
