package com.clockey.server.service;

import com.clockey.server.pojo.User;

public interface UserService {

	/**
	 * 保存用户
	 * @param label
	 * @return
	 */
	public long save(User user);
	
	/**
	 * 根据手机号码搜索用户
	 */
	public User getUserByPhone(String phone);
}
