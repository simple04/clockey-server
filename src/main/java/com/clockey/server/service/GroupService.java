package com.clockey.server.service;

import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.clockey.server.pojo.Group;

public interface GroupService {

	/**
	 * 保存分组
	 * @param group
	 */
	public void save (Group group);
	
	/**
	 * 获取列表
	 * @param map
	 * @return
	 */
	public JSONArray list(Map<String,Object>map);
	
	/**
	 * 删除分组
	 * @param map
	 */
	public void delete(Map<String,Object>map);
	
	/**
	 * 更新组名
	 * @param group
	 */
	public void update(Group group);
}
