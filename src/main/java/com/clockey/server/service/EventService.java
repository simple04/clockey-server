package com.clockey.server.service;

import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public interface EventService {

	/**
	 * 得到事件列表
	 * @param map
	 * @return
	 */
	public JSONArray getEventsByCondition(Map<String,Object>map);
	
	/**
	 * 发布事件
	 * @param map
	 */
	public void publishEvent(Map<String,Object>map);
	
	
	/**
	 * 得到该用户的事件
	 * @param map
	 * @return
	 */
	public JSONArray getPublisherEventsByUserId(Map<String,Object>map);
	
	
	/**
	 * 得到用户参与的事件
	 * @param map
	 * @return
	 */
	public JSONArray getParticipantEventsByUserId(Map<String,Object>map);
	
	/**
	 * 得到所有该事件的参与者
	 * @param eventId
	 * @return
	 */
	public JSONArray getEventParticipantsByEventId(long eventId);
	/**
	 * 得到该事件的详情
	 * @param id
	 * @return
	 */
	public JSONObject getEventById(Map<String,Object>map);
	
	/**
	 * 被邀请者处理该事件
	 * @param map
	 */
	public void handEventByInviter(Map<String,Object>map);
	
	/**
	 * 发布者删除该事件的邀请者
	 * @param map
	 */
	public void deleteInviterByPublisher(Map<String,Object>map);
	
	/**
	 * 发布者删除该事件的参加者
	 * @param map
	 */
	public void deleteParticipanterByPublisher(Map<String,Object>map);
	
	/**
	 * 参加此活动
	 * @param map
	 */
	public int participantEvent(Map<String,Object>map);
}
