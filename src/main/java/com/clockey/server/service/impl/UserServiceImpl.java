package com.clockey.server.service.impl;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clockey.server.dao.UserMapper;
import com.clockey.server.pojo.User;
import com.clockey.server.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;

	@Override
	public long save(User user) {
		return userMapper.insert(user);
	}

	@Override
	public User getUserByPhone(String phone) {
		return userMapper.select(new HashMap<String,Object>().put("phone", phone));
	}

}
