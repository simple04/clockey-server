package com.clockey.server.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.clockey.server.dao.GroupMapper;
import com.clockey.server.pojo.Group;
import com.clockey.server.service.GroupService;

@Service
public class GroupServiceImpl implements GroupService{

	
	@Autowired
	private GroupMapper groupMapper;
	
	@Override
	public void save(Group group) {
		groupMapper.insert(group);
	}

	@Override
	public JSONArray list(Map<String, Object> map) {
		JSONArray result = new JSONArray();
		List<Group> groups = groupMapper.list(map);
		for(Group group:groups){
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("userId", group.getUserId());
			jsonObject.put("name", group.getName());
			jsonObject.put("createTime", group.getCreateTime());
			jsonObject.put("id", group.getId());
			result.add(jsonObject);
		}
		return result;
	}

	@Override
	public void delete(Map<String, Object> map) {
		Object id = map.get("id");
		groupMapper.delete(id);
	}

	@Override
	public void update(Group group) {
		groupMapper.update(group);
	}

}
