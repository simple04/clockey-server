package com.clockey.server.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.clockey.server.dao.UserGroupMapper;
import com.clockey.server.dao.UserMapper;
import com.clockey.server.pojo.User;
import com.clockey.server.pojo.UserGroupRel;
import com.clockey.server.service.UserGroupService;

@Service
public class UserGroupServiceImpl implements UserGroupService{

	@Autowired
	private UserGroupMapper userGroupMapper;
	
	@Autowired
	private UserMapper userMapper;
	
	@SuppressWarnings("unchecked")
	@Override
	public void saveAll(Map<String, Object> map) {
		List<UserGroupRel> userGroupRels = new ArrayList<UserGroupRel>();
		long groupId =(Long) map.get("groupId");
		List<Long>userIds=(List<Long>) map.get("userIds");
		for(Long userId:userIds){
			UserGroupRel  userGroupRel = new UserGroupRel();
			userGroupRel.setUserId(userId);
			userGroupRel.setGroupId(groupId);
			userGroupRels.add(userGroupRel);
		}
		userGroupMapper.batchInsert(userGroupRels);
	}

	@Override
	public void deleteUserIds(Map<String, Object> map) {
		userGroupMapper.deleteByMap(map);
	}

	@Override
	public JSONArray getUsersByGroup(Map<String, Object> map) {
		JSONArray result = new JSONArray();
		List<UserGroupRel> userGroups=userGroupMapper.list(map);
		for(UserGroupRel userGroupRel : userGroups){
			JSONObject jsonObject = new JSONObject();
			User user = userMapper.select(userGroupRel.getUserId());
			jsonObject.put("avatar", user.getAvatar());
			jsonObject.put("phone", user.getPhone());
			jsonObject.put("userName", user.getUserName());
			result.add(jsonObject);
		}
		return result;
		
		
		
		
	}

}
