package com.clockey.server.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.clockey.server.dao.LabelMapper;
import com.clockey.server.pojo.Label;
import com.clockey.server.service.LabelService;

@Service
public class LabelServiceImpl implements LabelService {

	@Autowired
	private LabelMapper labelMapper;

	@Override
	public long save(Label label) {
		labelMapper.insert(label);
		return label.getId();
	}

	@Override
	public JSONArray findLabel(Map<String, Object> map) {
		JSONArray jsonArray = new JSONArray();
		List<Label> labels = labelMapper.list(map);
		if (labels != null && !labels.isEmpty()) {
			for (Label label : labels) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("name", label.getName());
				jsonObject.put("id", label.getId());
				jsonArray.add(jsonObject);
			}
		}
		return jsonArray;
	}

}
