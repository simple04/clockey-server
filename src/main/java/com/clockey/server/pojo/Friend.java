package com.clockey.server.pojo;

import java.util.Date;

public class Friend {

	/*用户id*/
	private Long userId;
	/*好友id*/
	private Long friendUserId;
	/*创建时间*/
	private Date createTime;
	/*备注*/
	private String remarks;
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getFriendUserId() {
		return friendUserId;
	}
	public void setFriendUserId(Long friendUserId) {
		this.friendUserId = friendUserId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
}
