package com.clockey.server.pojo;

import java.util.Date;

public class Group {
	/*id*/
	private long id;
    /*组名字*/
	private String name;
    /*创建世界*/
	private Date createTime;
	/*用户id*/
	private long userId;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	
}
