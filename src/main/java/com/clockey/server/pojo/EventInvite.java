package com.clockey.server.pojo;

import java.util.Date;

public class EventInvite {
	/*事件id*/
	private Long eventId;
	/*受邀用户id*/
	private Long  handUserId;
	/*邀请时间*/
	private Date createTime;
	/*处理时间*/
	private Date handTime;
	/*处理结果*/
	private int handResult;
	
	public Long getEventId() {
		return eventId;
	}
	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Long getHandUserId() {
		return handUserId;
	}
	public void setHandUserId(Long handUserId) {
		this.handUserId = handUserId;
	}
	public Date getHandTime() {
		return handTime;
	}
	public void setHandTime(Date handTime) {
		this.handTime = handTime;
	}
	public int getHandResult() {
		return handResult;
	}
	public void setHandResult(int handResult) {
		this.handResult = handResult;
	}
	
}
