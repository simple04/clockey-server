package com.clockey.server.pojo;

import java.util.Date;

public class FriendRequest {
	/*发送用户id*/
	private Long sendUserId;
	/*处理用户id*/
	private Long handUserId;
	/*发送时间*/
	private Date sendTime;
	/*处理时间*/
	private Date handTime;
	/*处理结果*/
	private Integer handResult;
	public Long getSendUserId() {
		return sendUserId;
	}
	public void setSendUserId(Long sendUserId) {
		this.sendUserId = sendUserId;
	}
	public Long getHandUserId() {
		return handUserId;
	}
	public void setHandUserId(Long handUserId) {
		this.handUserId = handUserId;
	}
	public Date getSendTime() {
		return sendTime;
	}
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	public Date getHandTime() {
		return handTime;
	}
	public void setHandTime(Date handTime) {
		this.handTime = handTime;
	}
	public Integer getHandResult() {
		return handResult;
	}
	public void setHandResult(Integer handResult) {
		this.handResult = handResult;
	}
	
}
