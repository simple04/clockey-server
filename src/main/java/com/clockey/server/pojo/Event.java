package com.clockey.server.pojo;

import java.util.Date;

public class Event {
	/*id*/
	private long id;
	/*活动图标*/
	private String icon;
	/*内容*/
	private String content;
	/*创建时间*/
	private Date createTime;
	/*活动开始时间*/
	private Date beginTime;
	/*活动结束时间*/
	private Date endTime;
	/*语音url*/
	private String soundUrl;
	/*地址*/
	private String location;
	/*图片地址*/
	private String picUrls;
	/*参与人数限制*/
	private int participantLimit;
	/*备选人员*/
	private int alternateNum;
	/*查看数*/
	private int viewCount;
	/*组队*/
	private int team;
	/*提前确认时间*/
	private int advanceTime;
	/*是否公开招募*/
	private int publishOffer;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getSoundUrl() {
		return soundUrl;
	}
	public void setSoundUrl(String soundUrl) {
		this.soundUrl = soundUrl;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getPicUrls() {
		return picUrls;
	}
	public void setPicUrls(String picUrls) {
		this.picUrls = picUrls;
	}
	public int getParticipantLimit() {
		return participantLimit;
	}
	public void setParticipantLimit(int participantLimit) {
		this.participantLimit = participantLimit;
	}
	public int getViewCount() {
		return viewCount;
	}
	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}
	public int getAlternateNum() {
		return alternateNum;
	}
	public void setAlternateNum(int alternateNum) {
		this.alternateNum = alternateNum;
	}
	public int getTeam() {
		return team;
	}
	public void setTeam(int team) {
		this.team = team;
	}
	public int getAdvanceTime() {
		return advanceTime;
	}
	public void setAdvanceTime(int advanceTime) {
		this.advanceTime = advanceTime;
	}
	public int getPublishOffer() {
		return publishOffer;
	}
	public void setPublishOffer(int publishOffer) {
		this.publishOffer = publishOffer;
	}
	
}
