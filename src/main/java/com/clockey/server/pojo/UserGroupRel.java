package com.clockey.server.pojo;

import java.util.Date;

public class UserGroupRel {

	/*组id*/
	private long groupId;
	/*用户id*/
	private long userId;
	/*创建世界*/
	private Date createTime;
	public long getGroupId() {
		return groupId;
	}
	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	
}
