package com.clockey.server.pojo;

import java.util.Date;

public class EventParticipant {
	/*事件id*/
	private long eventId;
	/*参与时间*/
	private Date createTime;
	/*管理员状态*/
	private int manageStatus;
	/*参加活动状态*/
	private int participanterStatus;
	/*参与者用户id*/
	private long participantUserId;
	/*参与者处理时间*/
	private Date participanteHandTime;
	/*管理者处理时间*/
	private Date managerHandTime;
	
	public long getEventId() {
		return eventId;
	}
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public int getManageStatus() {
		return manageStatus;
	}
	public void setManageStatus(int manageStatus) {
		this.manageStatus = manageStatus;
	}
	public int getParticipanterStatus() {
		return participanterStatus;
	}
	public void setParticipanterStatus(int participanterStatus) {
		this.participanterStatus = participanterStatus;
	}
	public long getParticipantUserId() {
		return participantUserId;
	}
	public void setParticipantUserId(long participantUserId) {
		this.participantUserId = participantUserId;
	}
	public Date getParticipanteHandTime() {
		return participanteHandTime;
	}
	public void setParticipanteHandTime(Date participanteHandTime) {
		this.participanteHandTime = participanteHandTime;
	}
	public Date getManagerHandTime() {
		return managerHandTime;
	}
	public void setManagerHandTime(Date managerHandTime) {
		this.managerHandTime = managerHandTime;
	}
	
}
