package com.clockey.server.pojo;

import java.util.Date;

public class Label {
	/*id*/
	private long id;
	/*标签名字*/
	private String name;
	/*创建世界*/
	private Date createTime;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
