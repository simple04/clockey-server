package com.clockey.server.pojo;

import java.util.Date;

public class LabelEventRel {
	/*标签id*/
	private long labelId;
	/*事件id*/
	private long eventId;
	/*创建时间*/
	private Date createTime;
	public long getLabelId() {
		return labelId;
	}
	public void setLabelId(long labelId) {
		this.labelId = labelId;
	}
	public long getEventId() {
		return eventId;
	}
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
}
