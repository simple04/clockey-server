package com.clockey.server.pojo;

public class EventPublisher {
	/* 事件id */
	private long eventId;

	/* 发布者用户id */
	private long userId;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getEventId() {
		return eventId;
	}

	public void setEventId(long eventId) {
		this.eventId = eventId;
	}

}
