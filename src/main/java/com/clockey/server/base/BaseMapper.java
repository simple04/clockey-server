package com.clockey.server.base;

import java.util.List;
import java.util.Map;

public interface BaseMapper<T> {
	/**
	 * 插入数据
	 * @param t
	 * @return
	 */
	public int insert(T t);
	
	/**
	 * 批量插入
	 * @param lists
	 */
	public void batchInsert(List<T>lists);
	
	/**
	 * 根据字段查询
	 * @param key
	 * @return
	 */
	public T select(Object key);
	
	/**
	 * 更新数据
	 * @param t
	 * @return
	 */
	public int update(T t);
	
	/**
	 * 多条件更新
	 * @param map
	 * @return
	 */
	public int update(Map<String,Object>map);
	
	/**
	 * 获取列表
	 * @param map
	 * @return
	 */
	public List<T> list(Map<String,Object>map);
	
	/**
	 * 根据id删除记录
	 * @param id
	 * @return
	 */
	public int delete(Object id);
	
	/**
	 * 根据map删除记录
	 * @param map
	 * @return
	 */
	public int deleteByMap(Map<String,Object>map);
	
	/**
	 * 根据条件查询记录数
	 * @param map
	 * @return
	 */
	public int count(Map<String,Object>map);
}
