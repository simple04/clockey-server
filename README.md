## clockey-server
基于netty4的轻量级http服务, 支持restful风格, 集合了spring, mybatis.
数据库是postgresql

#配置文件
/clockey-server/src/main/resources是配置文件存放目录。
/clockey-server/src/main/resources/db/mybatis-datasource.xml数据库配置
/clockey-server/src/main/resources/main/server.xml controller配置
/clockey-server/src/main/resources/logback.xml 日志配置
/clockey-server/src/main/resources/server.properties 数据库配置文件
/clockey-server/sql/tbl_clockey.sql sql语句

/clockey-server/src/main/java  主代码目录
/clockey-server/src/test/java  测试代码目录
/clockey-server/sql/tbl_clockey.sql sql语句
/clockey-server/lib  第三方jar一定要引入到项目中去

## 本地开发环境
1. 导入该git项目，成功下载jar包
2. 执行sql/install.sql,数据库为postgresql，修改server.properties数据库连接帐号和密码
3. 启动服务com.clockey.server.ServerStart,执行main方法
4. 测试controller则用TestController执行main方法.


## 打包部署
直接项目右键run as maven build,Goals里输入package,然后run
最终生成一个部署zip压缩包，直接解压执行脚本即可