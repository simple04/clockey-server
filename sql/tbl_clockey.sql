
CREATE SEQUENCE seq_tbl_user
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 99999999999999
  START 1
  CACHE 1;

CREATE TABLE tbl_user
(
  id numeric(19,0) NOT NULL, ---主键
  user_name character varying(64),
  user_pwd  character varying(64),
  avatar character varying(64),  --用户图标
  mobile_phone character varying(32),
  create_time timestamp without time zone default now(),
  login_time timestamp without time zone default now(),
  cid character varying(256), --clientId 个推客户端插件上传
  CONSTRAINT pk_tbl_user PRIMARY KEY (id)
);


CREATE SEQUENCE seq_tbl_event
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 99999999999999
  START 1
  CACHE 1;

CREATE TABLE tbl_event
(
  id numeric(19,0) NOT NULL, ---主键
  icon character varying(64),  --活动图标
  content character varying(512),  --活动内容
  create_time timestamp without time zone default now(),
  begin_time timestamp without time zone,
  end_time timestamp without time zone,
  sound_url character varying(64),
  location character varying(10),
  pic_urls character varying(512),
  participant_limit numeric(19,0) NOT NULL default 0,
  alternateNum numeric(10,0) not null default 0,--替补人数
  view_count numeric(10,0) not null default 0,
  team numeric(1,0) not null default 1, --0,否，1是
  advance_time numeric(3,0) not null default 10,
  public_offer numeric(1,0) not null default 1, --0,否，1是
  CONSTRAINT pk_tbl_event PRIMARY KEY (id)
);

create TABLE tbl_event_invite_rel
(
  event_id numeric(19,0) NOT NULL, ---主键
  user_id numeric(19,0) NOT NULL, ---主键
  hand_user_id numeric(19,0) NOT NULL, ---主键
  create_time timestamp without time zone default now(),
  hand_time timestamp default null,
  hand_result numeric(1,0) not null default 0 ---0 未处理 1同意 2拒绝
);

CREATE TABLE tbl_event_publisher_rel
(
  event_id numeric(19,0) NOT NULL, ---主键
  user_id numeric(19,0) NOT NULL, ---主键
  create_time timestamp without time zone default now()
);

CREATE TABLE tbl_event_participant_rel
(
  event_id numeric(19,0) NOT NULL, ---主键
  create_time timestamp without time zone default now(),
  participanter_handtime timestamp default null,
  manager_handtime timestamp default null,
  manage_status numeric(1,0) not null default 0, ---0 一般  1管理员  2替补 
  participant_userId numeric(19,0) not null,
  participanter_status numeric(1,0) not null default 0 --0 一般 ，1未确认， 2，确认 
);


CREATE SEQUENCE seq_tbl_label
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 99999999999999
  START 1
  CACHE 1;

CREATE TABLE tbl_label
(
  id numeric(19,0) NOT NULL, ---主键
  name character varying(64),  --名字
  create_time timestamp without time zone default now(),
  CONSTRAINT pk_tbl_label PRIMARY KEY (id)
);

CREATE SEQUENCE seq_tbl_group
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 99999999999999
  START 1
  CACHE 1;
  
CREATE table tbl_group
(
  id numeric(19,0) NOT NULL, ---主键
  name character varying(64),  --名字
  user_id numeric(19,0) NOT NULL,
  create_time timestamp without time zone default now(),
  CONSTRAINT pk_tbl_group PRIMARY KEY (id)
);

CREATE TABLE tbl_label_event_rel
(
  label_id numeric(19,0) NOT NULL, ---主键
  event_id numeric(19,0) NOT NULL, ---主键
  create_time timestamp without time zone default now()
);

CREATE TABLE tbl_group_user_rel
(
  group_id numeric(19,0) NOT NULL, ---主键
  user_id numeric(19,0) NOT NULL, ---主键
  create_time timestamp without time zone default now()
);

create table tbl_friend(
	user_id numeric(19,0) NOT NULL,
	friend_id numeric(19,0) NOT NULL,
	create_time timestamp without time zone default now(),
	remarks character varying(64)
);

CREATE SEQUENCE seq_tbl_friend_req
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 99999999999999
  START 1
  CACHE 1;

create table tbl_friend_req(
    id numeric(19,0) NOT NULL, ---主键
    send_req_user_id numeric(19,0) NOT NULL,
    hand_userId numeric(19,0) NOT NULL,
    send_req_time timestamp without time zone default now(),
    hand_req_time timestamp default null,
    hand_result numeric(1,0) not null default 0 ---0 未处理 1同意 2拒绝
);




